package com.corerights.license.repository;

import com.corerights.license.domain.LineItemOptions;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LineItemOptions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LineItemOptionsRepository extends JpaRepository<LineItemOptions, Long> {

}
