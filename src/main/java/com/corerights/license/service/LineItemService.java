package com.corerights.license.service;

import com.corerights.license.service.dto.LineItemDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing LineItem.
 */
public interface LineItemService {

    /**
     * Save a lineItem.
     *
     * @param lineItemDTO the entity to save
     * @return the persisted entity
     */
    LineItemDTO save(LineItemDTO lineItemDTO);

    /**
     * Get all the lineItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<LineItemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" lineItem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    LineItemDTO findOne(Long id);

    /**
     * Delete the "id" lineItem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
