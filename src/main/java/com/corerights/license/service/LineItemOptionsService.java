package com.corerights.license.service;

import com.corerights.license.service.dto.LineItemOptionsDTO;
import java.util.List;

/**
 * Service Interface for managing LineItemOptions.
 */
public interface LineItemOptionsService {

    /**
     * Save a lineItemOptions.
     *
     * @param lineItemOptionsDTO the entity to save
     * @return the persisted entity
     */
    LineItemOptionsDTO save(LineItemOptionsDTO lineItemOptionsDTO);

    /**
     * Get all the lineItemOptions.
     *
     * @return the list of entities
     */
    List<LineItemOptionsDTO> findAll();

    /**
     * Get the "id" lineItemOptions.
     *
     * @param id the id of the entity
     * @return the entity
     */
    LineItemOptionsDTO findOne(Long id);

    /**
     * Delete the "id" lineItemOptions.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
