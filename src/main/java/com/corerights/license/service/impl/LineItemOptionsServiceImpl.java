package com.corerights.license.service.impl;

import com.corerights.license.service.LineItemOptionsService;
import com.corerights.license.domain.LineItemOptions;
import com.corerights.license.repository.LineItemOptionsRepository;
import com.corerights.license.service.dto.LineItemOptionsDTO;
import com.corerights.license.service.mapper.LineItemOptionsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing LineItemOptions.
 */
@Service
@Transactional
public class LineItemOptionsServiceImpl implements LineItemOptionsService{

    private final Logger log = LoggerFactory.getLogger(LineItemOptionsServiceImpl.class);

    private final LineItemOptionsRepository lineItemOptionsRepository;

    private final LineItemOptionsMapper lineItemOptionsMapper;

    public LineItemOptionsServiceImpl(LineItemOptionsRepository lineItemOptionsRepository, LineItemOptionsMapper lineItemOptionsMapper) {
        this.lineItemOptionsRepository = lineItemOptionsRepository;
        this.lineItemOptionsMapper = lineItemOptionsMapper;
    }

    /**
     * Save a lineItemOptions.
     *
     * @param lineItemOptionsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LineItemOptionsDTO save(LineItemOptionsDTO lineItemOptionsDTO) {
        log.debug("Request to save LineItemOptions : {}", lineItemOptionsDTO);
        LineItemOptions lineItemOptions = lineItemOptionsMapper.toEntity(lineItemOptionsDTO);
        lineItemOptions = lineItemOptionsRepository.save(lineItemOptions);
        return lineItemOptionsMapper.toDto(lineItemOptions);
    }

    /**
     * Get all the lineItemOptions.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LineItemOptionsDTO> findAll() {
        log.debug("Request to get all LineItemOptions");
        return lineItemOptionsRepository.findAll().stream()
            .map(lineItemOptionsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one lineItemOptions by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LineItemOptionsDTO findOne(Long id) {
        log.debug("Request to get LineItemOptions : {}", id);
        LineItemOptions lineItemOptions = lineItemOptionsRepository.findOne(id);
        return lineItemOptionsMapper.toDto(lineItemOptions);
    }

    /**
     * Delete the lineItemOptions by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LineItemOptions : {}", id);
        lineItemOptionsRepository.delete(id);
    }
}
