package com.corerights.license.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.corerights.license.domain.License;
import com.corerights.license.repository.LicenseRepository;
import com.corerights.license.service.LicenseService;
import com.corerights.license.service.dto.LicenseDTO;
import com.corerights.license.service.mapper.LicenseMappr;

/**
 * Service Implementation for managing License.
 */
@Service
@Transactional
public class LicenseServiceImpl implements LicenseService{

    private final Logger log = LoggerFactory.getLogger(LicenseServiceImpl.class);

    private final LicenseRepository licenseRepository;

    private final LicenseMappr licenseMapper;

    public LicenseServiceImpl(LicenseRepository licenseRepository, LicenseMappr licenseMapper) {
        this.licenseRepository = licenseRepository;
        this.licenseMapper = licenseMapper;
    }

    /**
     * Save a license.
     *
     * @param licenseDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LicenseDTO save(LicenseDTO licenseDTO) {
        log.debug("Request to save License : {}", licenseDTO);
        License license = licenseMapper.toEntity(licenseDTO);
        license = licenseRepository.save(license);
        return licenseMapper.toDto(license);
    }

    /**
     * Get all the licenses.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LicenseDTO> findAll() {
        log.debug("Request to get all Licenses");
        return licenseRepository.findAll().stream()
            .map(licenseMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one license by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LicenseDTO findOne(Long id) {
        log.debug("Request to get License : {}", id);
        License license = licenseRepository.findOne(id);
        return licenseMapper.toDto(license);
    }

    /**
     * Delete the license by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete License : {}", id);
        licenseRepository.delete(id);
    }
}
