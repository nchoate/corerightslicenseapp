package com.corerights.license.service.impl;

import com.corerights.license.service.LineItemService;
import com.corerights.license.domain.LineItem;
import com.corerights.license.repository.LineItemRepository;
import com.corerights.license.service.dto.LineItemDTO;
import com.corerights.license.service.mapper.LineItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing LineItem.
 */
@Service
@Transactional
public class LineItemServiceImpl implements LineItemService{

    private final Logger log = LoggerFactory.getLogger(LineItemServiceImpl.class);

    private final LineItemRepository lineItemRepository;

    private final LineItemMapper lineItemMapper;

    public LineItemServiceImpl(LineItemRepository lineItemRepository, LineItemMapper lineItemMapper) {
        this.lineItemRepository = lineItemRepository;
        this.lineItemMapper = lineItemMapper;
    }

    /**
     * Save a lineItem.
     *
     * @param lineItemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LineItemDTO save(LineItemDTO lineItemDTO) {
        log.debug("Request to save LineItem : {}", lineItemDTO);
        LineItem lineItem = lineItemMapper.toEntity(lineItemDTO);
        lineItem = lineItemRepository.save(lineItem);
        return lineItemMapper.toDto(lineItem);
    }

    /**
     * Get all the lineItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LineItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LineItems");
        return lineItemRepository.findAll(pageable)
            .map(lineItemMapper::toDto);
    }

    /**
     * Get one lineItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LineItemDTO findOne(Long id) {
        log.debug("Request to get LineItem : {}", id);
        LineItem lineItem = lineItemRepository.findOne(id);
        return lineItemMapper.toDto(lineItem);
    }

    /**
     * Delete the lineItem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LineItem : {}", id);
        lineItemRepository.delete(id);
    }
}
