package com.corerights.license.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the License entity.
 */
public class LicenseDTO implements Serializable {

    private Long id;

    private String licenseId;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LicenseDTO licenseDTO = (LicenseDTO) o;
        if(licenseDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), licenseDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LicenseDTO{" +
            "id=" + getId() +
            ", licenseId='" + getLicenseId() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
