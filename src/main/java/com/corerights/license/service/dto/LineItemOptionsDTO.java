package com.corerights.license.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the LineItemOptions entity.
 */
public class LineItemOptionsDTO implements Serializable {

    private Long id;

    private String lineItemOptionId;

    private String name;

    private String helpDesc;

    private Integer ord;

    private Boolean selected;

    private Long lineItemId;

    private String lineItemName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLineItemOptionId() {
        return lineItemOptionId;
    }

    public void setLineItemOptionId(String lineItemOptionId) {
        this.lineItemOptionId = lineItemOptionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpDesc() {
        return helpDesc;
    }

    public void setHelpDesc(String helpDesc) {
        this.helpDesc = helpDesc;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public Boolean isSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Long getLineItemId() {
        return lineItemId;
    }

    public void setLineItemId(Long lineItemId) {
        this.lineItemId = lineItemId;
    }

    public String getLineItemName() {
        return lineItemName;
    }

    public void setLineItemName(String lineItemName) {
        this.lineItemName = lineItemName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LineItemOptionsDTO lineItemOptionsDTO = (LineItemOptionsDTO) o;
        if(lineItemOptionsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lineItemOptionsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LineItemOptionsDTO{" +
            "id=" + getId() +
            ", lineItemOptionId='" + getLineItemOptionId() + "'" +
            ", name='" + getName() + "'" +
            ", helpDesc='" + getHelpDesc() + "'" +
            ", ord=" + getOrd() +
            ", selected='" + isSelected() + "'" +
            "}";
    }
}
