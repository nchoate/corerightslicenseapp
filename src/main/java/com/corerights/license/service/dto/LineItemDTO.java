package com.corerights.license.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.corerights.license.domain.enumeration.InputType;

/**
 * A DTO for the LineItem entity.
 */
public class LineItemDTO implements Serializable {

    private Long id;

    private String lineItemId;

    private String name;

    private String helpDesc;

    private InputType inputType;

    private String userResponse;

    private Long sectionId;

    private String sectionName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLineItemId() {
        return lineItemId;
    }

    public void setLineItemId(String lineItemId) {
        this.lineItemId = lineItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpDesc() {
        return helpDesc;
    }

    public void setHelpDesc(String helpDesc) {
        this.helpDesc = helpDesc;
    }

    public InputType getInputType() {
        return inputType;
    }

    public void setInputType(InputType inputType) {
        this.inputType = inputType;
    }

    public String getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(String userResponse) {
        this.userResponse = userResponse;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LineItemDTO lineItemDTO = (LineItemDTO) o;
        if(lineItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lineItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LineItemDTO{" +
            "id=" + getId() +
            ", lineItemId='" + getLineItemId() + "'" +
            ", name='" + getName() + "'" +
            ", helpDesc='" + getHelpDesc() + "'" +
            ", inputType='" + getInputType() + "'" +
            ", userResponse='" + getUserResponse() + "'" +
            "}";
    }
}
