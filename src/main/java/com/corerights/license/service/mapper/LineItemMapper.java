package com.corerights.license.service.mapper;

import com.corerights.license.domain.*;
import com.corerights.license.service.dto.LineItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LineItem and its DTO LineItemDTO.
 */
@Mapper(componentModel = "spring", uses = {SectionMapper.class})
public interface LineItemMapper extends EntityMapper<LineItemDTO, LineItem> {

    @Mapping(source = "section.id", target = "sectionId")
    @Mapping(source = "section.name", target = "sectionName")
    LineItemDTO toDto(LineItem lineItem); 

    @Mapping(target = "options", ignore = true)
    @Mapping(source = "sectionId", target = "section")
    LineItem toEntity(LineItemDTO lineItemDTO);

    default LineItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        LineItem lineItem = new LineItem();
        lineItem.setId(id);
        return lineItem;
    }
}
