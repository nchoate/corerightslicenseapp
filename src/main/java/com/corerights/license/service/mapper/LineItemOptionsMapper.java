package com.corerights.license.service.mapper;

import com.corerights.license.domain.*;
import com.corerights.license.service.dto.LineItemOptionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LineItemOptions and its DTO LineItemOptionsDTO.
 */
@Mapper(componentModel = "spring", uses = {LineItemMapper.class})
public interface LineItemOptionsMapper extends EntityMapper<LineItemOptionsDTO, LineItemOptions> {

    @Mapping(source = "lineItem.id", target = "lineItemId")
    @Mapping(source = "lineItem.name", target = "lineItemName")
    LineItemOptionsDTO toDto(LineItemOptions lineItemOptions); 

    @Mapping(source = "lineItemId", target = "lineItem")
    LineItemOptions toEntity(LineItemOptionsDTO lineItemOptionsDTO);

    default LineItemOptions fromId(Long id) {
        if (id == null) {
            return null;
        }
        LineItemOptions lineItemOptions = new LineItemOptions();
        lineItemOptions.setId(id);
        return lineItemOptions;
    }
}
