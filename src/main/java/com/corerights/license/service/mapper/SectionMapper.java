package com.corerights.license.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.corerights.license.domain.Section;
import com.corerights.license.service.dto.SectionDTO;

/**
 * Mapper for the entity Section and its DTO SectionDTO.
 */
@Mapper(componentModel = "spring", uses = {LicenseMappr.class})
public interface SectionMapper extends EntityMapper<SectionDTO, Section> {

    @Mapping(source = "license.id", target = "licenseId")
    @Mapping(source = "license.name", target = "licenseName")
    SectionDTO toDto(Section section); 

    @Mapping(source = "licenseId", target = "license")
    Section toEntity(SectionDTO sectionDTO);

    default Section fromId(Long id) {
        if (id == null) {
            return null;
        }
        Section section = new Section();
        section.setId(id);
        return section;
    }
}
