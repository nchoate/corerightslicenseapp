package com.corerights.license.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Qualifier;

import com.corerights.license.domain.License;
import com.corerights.license.service.dto.LicenseDTO;

/**
 * Mapper for the entity License and its DTO LicenseDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LicenseMappr extends EntityMapper<LicenseDTO, License> {

    

    

    default License fromId(Long id) {
        if (id == null) {
            return null;
        }
        License license = new License();
        license.setId(id);
        return license;
    }
}
