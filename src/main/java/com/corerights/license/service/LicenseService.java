package com.corerights.license.service;

import com.corerights.license.service.dto.LicenseDTO;
import java.util.List;

/**
 * Service Interface for managing License.
 */
public interface LicenseService {

    /**
     * Save a license.
     *
     * @param licenseDTO the entity to save
     * @return the persisted entity
     */
    LicenseDTO save(LicenseDTO licenseDTO);

    /**
     * Get all the licenses.
     *
     * @return the list of entities
     */
    List<LicenseDTO> findAll();

    /**
     * Get the "id" license.
     *
     * @param id the id of the entity
     * @return the entity
     */
    LicenseDTO findOne(Long id);

    /**
     * Delete the "id" license.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
