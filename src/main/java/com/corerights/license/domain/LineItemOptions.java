package com.corerights.license.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LineItemOptions.
 */
@Entity
@Table(name = "line_item_options")
public class LineItemOptions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "line_item_option_id")
    private String lineItemOptionId;

    @Column(name = "name")
    private String name;

    @Column(name = "help_desc")
    private String helpDesc;

    @Column(name = "ord")
    private Integer ord;

    @Column(name = "selected")
    private Boolean selected;

    @ManyToOne
    private LineItem lineItem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLineItemOptionId() {
        return lineItemOptionId;
    }

    public LineItemOptions lineItemOptionId(String lineItemOptionId) {
        this.lineItemOptionId = lineItemOptionId;
        return this;
    }

    public void setLineItemOptionId(String lineItemOptionId) {
        this.lineItemOptionId = lineItemOptionId;
    }

    public String getName() {
        return name;
    }

    public LineItemOptions name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpDesc() {
        return helpDesc;
    }

    public LineItemOptions helpDesc(String helpDesc) {
        this.helpDesc = helpDesc;
        return this;
    }

    public void setHelpDesc(String helpDesc) {
        this.helpDesc = helpDesc;
    }

    public Integer getOrd() {
        return ord;
    }

    public LineItemOptions ord(Integer ord) {
        this.ord = ord;
        return this;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public Boolean isSelected() {
        return selected;
    }

    public LineItemOptions selected(Boolean selected) {
        this.selected = selected;
        return this;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public LineItem getLineItem() {
        return lineItem;
    }

    public LineItemOptions lineItem(LineItem lineItem) {
        this.lineItem = lineItem;
        return this;
    }

    public void setLineItem(LineItem lineItem) {
        this.lineItem = lineItem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LineItemOptions lineItemOptions = (LineItemOptions) o;
        if (lineItemOptions.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lineItemOptions.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LineItemOptions{" +
            "id=" + getId() +
            ", lineItemOptionId='" + getLineItemOptionId() + "'" +
            ", name='" + getName() + "'" +
            ", helpDesc='" + getHelpDesc() + "'" +
            ", ord=" + getOrd() +
            ", selected='" + isSelected() + "'" +
            "}";
    }
}
