package com.corerights.license.domain.enumeration;

/**
 * The InputType enumeration.
 */
public enum InputType {
    TEXT, SINGLE_SELECT, MULTI_SELECT
}
