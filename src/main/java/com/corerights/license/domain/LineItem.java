package com.corerights.license.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.corerights.license.domain.enumeration.InputType;

/**
 * A LineItem.
 */
@Entity
@Table(name = "line_item")
public class LineItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "line_item_id")
    private String lineItemId;

    @Column(name = "name")
    private String name;

    @Column(name = "help_desc")
    private String helpDesc;

    @Enumerated(EnumType.STRING)
    @Column(name = "input_type")
    private InputType inputType;

    @Column(name = "user_response")
    private String userResponse;

    @OneToMany(mappedBy = "lineItem")
    @JsonIgnore
    private Set<LineItemOptions> options = new HashSet<>();

    @ManyToOne
    private Section section;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLineItemId() {
        return lineItemId;
    }

    public LineItem lineItemId(String lineItemId) {
        this.lineItemId = lineItemId;
        return this;
    }

    public void setLineItemId(String lineItemId) {
        this.lineItemId = lineItemId;
    }

    public String getName() {
        return name;
    }

    public LineItem name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpDesc() {
        return helpDesc;
    }

    public LineItem helpDesc(String helpDesc) {
        this.helpDesc = helpDesc;
        return this;
    }

    public void setHelpDesc(String helpDesc) {
        this.helpDesc = helpDesc;
    }

    public InputType getInputType() {
        return inputType;
    }

    public LineItem inputType(InputType inputType) {
        this.inputType = inputType;
        return this;
    }

    public void setInputType(InputType inputType) {
        this.inputType = inputType;
    }

    public String getUserResponse() {
        return userResponse;
    }

    public LineItem userResponse(String userResponse) {
        this.userResponse = userResponse;
        return this;
    }

    public void setUserResponse(String userResponse) {
        this.userResponse = userResponse;
    }

    public Set<LineItemOptions> getOptions() {
        return options;
    }

    public LineItem options(Set<LineItemOptions> lineItemOptions) {
        this.options = lineItemOptions;
        return this;
    }

    public LineItem addOptions(LineItemOptions lineItemOptions) {
        this.options.add(lineItemOptions);
        lineItemOptions.setLineItem(this);
        return this;
    }

    public LineItem removeOptions(LineItemOptions lineItemOptions) {
        this.options.remove(lineItemOptions);
        lineItemOptions.setLineItem(null);
        return this;
    }

    public void setOptions(Set<LineItemOptions> lineItemOptions) {
        this.options = lineItemOptions;
    }

    public Section getSection() {
        return section;
    }

    public LineItem section(Section section) {
        this.section = section;
        return this;
    }

    public void setSection(Section section) {
        this.section = section;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LineItem lineItem = (LineItem) o;
        if (lineItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lineItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LineItem{" +
            "id=" + getId() +
            ", lineItemId='" + getLineItemId() + "'" +
            ", name='" + getName() + "'" +
            ", helpDesc='" + getHelpDesc() + "'" +
            ", inputType='" + getInputType() + "'" +
            ", userResponse='" + getUserResponse() + "'" +
            "}";
    }
}
