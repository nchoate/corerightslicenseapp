/**
 * View Models used by Spring MVC REST controllers.
 */
package com.corerights.license.web.rest.vm;
