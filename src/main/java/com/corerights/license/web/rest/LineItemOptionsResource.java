package com.corerights.license.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.corerights.license.service.LineItemOptionsService;
import com.corerights.license.web.rest.errors.BadRequestAlertException;
import com.corerights.license.web.rest.util.HeaderUtil;
import com.corerights.license.service.dto.LineItemOptionsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LineItemOptions.
 */
@RestController
@RequestMapping("/api")
public class LineItemOptionsResource {

    private final Logger log = LoggerFactory.getLogger(LineItemOptionsResource.class);

    private static final String ENTITY_NAME = "lineItemOptions";

    private final LineItemOptionsService lineItemOptionsService;

    public LineItemOptionsResource(LineItemOptionsService lineItemOptionsService) {
        this.lineItemOptionsService = lineItemOptionsService;
    }

    /**
     * POST  /line-item-options : Create a new lineItemOptions.
     *
     * @param lineItemOptionsDTO the lineItemOptionsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lineItemOptionsDTO, or with status 400 (Bad Request) if the lineItemOptions has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/line-item-options")
    @Timed
    public ResponseEntity<LineItemOptionsDTO> createLineItemOptions(@RequestBody LineItemOptionsDTO lineItemOptionsDTO) throws URISyntaxException {
        log.debug("REST request to save LineItemOptions : {}", lineItemOptionsDTO);
        if (lineItemOptionsDTO.getId() != null) {
            throw new BadRequestAlertException("A new lineItemOptions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LineItemOptionsDTO result = lineItemOptionsService.save(lineItemOptionsDTO);
        return ResponseEntity.created(new URI("/api/line-item-options/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /line-item-options : Updates an existing lineItemOptions.
     *
     * @param lineItemOptionsDTO the lineItemOptionsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lineItemOptionsDTO,
     * or with status 400 (Bad Request) if the lineItemOptionsDTO is not valid,
     * or with status 500 (Internal Server Error) if the lineItemOptionsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/line-item-options")
    @Timed
    public ResponseEntity<LineItemOptionsDTO> updateLineItemOptions(@RequestBody LineItemOptionsDTO lineItemOptionsDTO) throws URISyntaxException {
        log.debug("REST request to update LineItemOptions : {}", lineItemOptionsDTO);
        if (lineItemOptionsDTO.getId() == null) {
            return createLineItemOptions(lineItemOptionsDTO);
        }
        LineItemOptionsDTO result = lineItemOptionsService.save(lineItemOptionsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lineItemOptionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /line-item-options : get all the lineItemOptions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of lineItemOptions in body
     */
    @GetMapping("/line-item-options")
    @Timed
    public List<LineItemOptionsDTO> getAllLineItemOptions() {
        log.debug("REST request to get all LineItemOptions");
        return lineItemOptionsService.findAll();
        }

    /**
     * GET  /line-item-options/:id : get the "id" lineItemOptions.
     *
     * @param id the id of the lineItemOptionsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lineItemOptionsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/line-item-options/{id}")
    @Timed
    public ResponseEntity<LineItemOptionsDTO> getLineItemOptions(@PathVariable Long id) {
        log.debug("REST request to get LineItemOptions : {}", id);
        LineItemOptionsDTO lineItemOptionsDTO = lineItemOptionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lineItemOptionsDTO));
    }

    /**
     * DELETE  /line-item-options/:id : delete the "id" lineItemOptions.
     *
     * @param id the id of the lineItemOptionsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/line-item-options/{id}")
    @Timed
    public ResponseEntity<Void> deleteLineItemOptions(@PathVariable Long id) {
        log.debug("REST request to delete LineItemOptions : {}", id);
        lineItemOptionsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
