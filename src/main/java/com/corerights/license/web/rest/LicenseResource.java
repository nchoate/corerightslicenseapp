package com.corerights.license.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.corerights.license.service.LicenseService;
import com.corerights.license.web.rest.errors.BadRequestAlertException;
import com.corerights.license.web.rest.util.HeaderUtil;
import com.corerights.license.service.dto.LicenseDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing License.
 */
@RestController
@RequestMapping("/api")
public class LicenseResource {

    private final Logger log = LoggerFactory.getLogger(LicenseResource.class);

    private static final String ENTITY_NAME = "license";

    private final LicenseService licenseService;

    public LicenseResource(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    /**
     * POST  /licenses : Create a new license.
     *
     * @param licenseDTO the licenseDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new licenseDTO, or with status 400 (Bad Request) if the license has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/licenses")
    @Timed
    public ResponseEntity<LicenseDTO> createLicense(@RequestBody LicenseDTO licenseDTO) throws URISyntaxException {
        log.debug("REST request to save License : {}", licenseDTO);
        if (licenseDTO.getId() != null) {
            throw new BadRequestAlertException("A new license cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LicenseDTO result = licenseService.save(licenseDTO);
        return ResponseEntity.created(new URI("/api/licenses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /licenses : Updates an existing license.
     *
     * @param licenseDTO the licenseDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated licenseDTO,
     * or with status 400 (Bad Request) if the licenseDTO is not valid,
     * or with status 500 (Internal Server Error) if the licenseDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/licenses")
    @Timed
    public ResponseEntity<LicenseDTO> updateLicense(@RequestBody LicenseDTO licenseDTO) throws URISyntaxException {
        log.debug("REST request to update License : {}", licenseDTO);
        if (licenseDTO.getId() == null) {
            return createLicense(licenseDTO);
        }
        LicenseDTO result = licenseService.save(licenseDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, licenseDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /licenses : get all the licenses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of licenses in body
     */
    @GetMapping("/licenses")
    @Timed
    public List<LicenseDTO> getAllLicenses() {
        log.debug("REST request to get all Licenses");
        return licenseService.findAll();
        }

    /**
     * GET  /licenses/:id : get the "id" license.
     *
     * @param id the id of the licenseDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the licenseDTO, or with status 404 (Not Found)
     */
    @GetMapping("/licenses/{id}")
    @Timed
    public ResponseEntity<LicenseDTO> getLicense(@PathVariable Long id) {
        log.debug("REST request to get License : {}", id);
        LicenseDTO licenseDTO = licenseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(licenseDTO));
    }

    /**
     * DELETE  /licenses/:id : delete the "id" license.
     *
     * @param id the id of the licenseDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/licenses/{id}")
    @Timed
    public ResponseEntity<Void> deleteLicense(@PathVariable Long id) {
        log.debug("REST request to delete License : {}", id);
        licenseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
