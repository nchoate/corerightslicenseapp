import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ngx-webstorage';

import { CoreRightsLicenseAppSharedModule, UserRouteAccessService } from './shared';
import { CoreRightsLicenseAppAppRoutingModule} from './app-routing.module';
import { CoreRightsLicenseAppHomeModule } from './home/home.module';
import { CoreRightsLicenseAppAdminModule } from './admin/admin.module';
import { CoreRightsLicenseAppAccountModule } from './account/account.module';
import { CoreRightsLicenseAppEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        CoreRightsLicenseAppAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        CoreRightsLicenseAppSharedModule,
        CoreRightsLicenseAppHomeModule,
        CoreRightsLicenseAppAdminModule,
        CoreRightsLicenseAppAccountModule,
        CoreRightsLicenseAppEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class CoreRightsLicenseAppAppModule {}
