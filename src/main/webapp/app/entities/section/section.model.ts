import { BaseEntity } from './../../shared';

export class Section implements BaseEntity {
    constructor(
        public id?: number,
        public sectionId?: string,
        public name?: string,
        public helpDesc?: string,
        public ord?: number,
        public licenseId?: number,
    ) {
    }
}
