import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreRightsLicenseAppSharedModule } from '../../shared';
import {
    LineItemService,
    LineItemPopupService,
    LineItemComponent,
    LineItemDetailComponent,
    LineItemDialogComponent,
    LineItemPopupComponent,
    LineItemDeletePopupComponent,
    LineItemDeleteDialogComponent,
    lineItemRoute,
    lineItemPopupRoute,
} from './';

const ENTITY_STATES = [
    ...lineItemRoute,
    ...lineItemPopupRoute,
];

@NgModule({
    imports: [
        CoreRightsLicenseAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LineItemComponent,
        LineItemDetailComponent,
        LineItemDialogComponent,
        LineItemDeleteDialogComponent,
        LineItemPopupComponent,
        LineItemDeletePopupComponent,
    ],
    entryComponents: [
        LineItemComponent,
        LineItemDialogComponent,
        LineItemPopupComponent,
        LineItemDeleteDialogComponent,
        LineItemDeletePopupComponent,
    ],
    providers: [
        LineItemService,
        LineItemPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreRightsLicenseAppLineItemModule {}
