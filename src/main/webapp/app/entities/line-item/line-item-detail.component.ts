import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LineItem } from './line-item.model';
import { LineItemService } from './line-item.service';

@Component({
    selector: 'jhi-line-item-detail',
    templateUrl: './line-item-detail.component.html'
})
export class LineItemDetailComponent implements OnInit, OnDestroy {

    lineItem: LineItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private lineItemService: LineItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLineItems();
    }

    load(id) {
        this.lineItemService.find(id).subscribe((lineItem) => {
            this.lineItem = lineItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLineItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'lineItemListModification',
            (response) => this.load(this.lineItem.id)
        );
    }
}
