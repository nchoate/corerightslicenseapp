import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LineItem } from './line-item.model';
import { LineItemPopupService } from './line-item-popup.service';
import { LineItemService } from './line-item.service';
import { Section, SectionService } from '../section';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-line-item-dialog',
    templateUrl: './line-item-dialog.component.html'
})
export class LineItemDialogComponent implements OnInit {

    lineItem: LineItem;
    isSaving: boolean;

    sections: Section[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private lineItemService: LineItemService,
        private sectionService: SectionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sectionService.query()
            .subscribe((res: ResponseWrapper) => { this.sections = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lineItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lineItemService.update(this.lineItem));
        } else {
            this.subscribeToSaveResponse(
                this.lineItemService.create(this.lineItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<LineItem>) {
        result.subscribe((res: LineItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LineItem) {
        this.eventManager.broadcast({ name: 'lineItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSectionById(index: number, item: Section) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-line-item-popup',
    template: ''
})
export class LineItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lineItemPopupService: LineItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lineItemPopupService
                    .open(LineItemDialogComponent as Component, params['id']);
            } else {
                this.lineItemPopupService
                    .open(LineItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
