import { BaseEntity } from './../../shared';

export const enum InputType {
    'TEXT',
    'SINGLE_SELECT',
    'MULTI_SELECT'
}

export class LineItem implements BaseEntity {
    constructor(
        public id?: number,
        public lineItemId?: string,
        public name?: string,
        public helpDesc?: string,
        public inputType?: InputType,
        public userResponse?: string,
        public options?: BaseEntity[],
        public sectionId?: number,
    ) {
    }
}
