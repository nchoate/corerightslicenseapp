import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { LineItemComponent } from './line-item.component';
import { LineItemDetailComponent } from './line-item-detail.component';
import { LineItemPopupComponent } from './line-item-dialog.component';
import { LineItemDeletePopupComponent } from './line-item-delete-dialog.component';

export const lineItemRoute: Routes = [
    {
        path: 'line-item',
        component: LineItemComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItems'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'line-item/:id',
        component: LineItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItems'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lineItemPopupRoute: Routes = [
    {
        path: 'line-item-new',
        component: LineItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItems'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'line-item/:id/edit',
        component: LineItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItems'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'line-item/:id/delete',
        component: LineItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItems'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
