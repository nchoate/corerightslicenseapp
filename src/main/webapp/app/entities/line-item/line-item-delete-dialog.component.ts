import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LineItem } from './line-item.model';
import { LineItemPopupService } from './line-item-popup.service';
import { LineItemService } from './line-item.service';

@Component({
    selector: 'jhi-line-item-delete-dialog',
    templateUrl: './line-item-delete-dialog.component.html'
})
export class LineItemDeleteDialogComponent {

    lineItem: LineItem;

    constructor(
        private lineItemService: LineItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lineItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'lineItemListModification',
                content: 'Deleted an lineItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-line-item-delete-popup',
    template: ''
})
export class LineItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lineItemPopupService: LineItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.lineItemPopupService
                .open(LineItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
