import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LineItemOptions } from './line-item-options.model';
import { LineItemOptionsService } from './line-item-options.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-line-item-options',
    templateUrl: './line-item-options.component.html'
})
export class LineItemOptionsComponent implements OnInit, OnDestroy {
lineItemOptions: LineItemOptions[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private lineItemOptionsService: LineItemOptionsService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.lineItemOptionsService.query().subscribe(
            (res: ResponseWrapper) => {
                this.lineItemOptions = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLineItemOptions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: LineItemOptions) {
        return item.id;
    }
    registerChangeInLineItemOptions() {
        this.eventSubscriber = this.eventManager.subscribe('lineItemOptionsListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
