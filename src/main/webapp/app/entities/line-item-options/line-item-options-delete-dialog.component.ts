import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LineItemOptions } from './line-item-options.model';
import { LineItemOptionsPopupService } from './line-item-options-popup.service';
import { LineItemOptionsService } from './line-item-options.service';

@Component({
    selector: 'jhi-line-item-options-delete-dialog',
    templateUrl: './line-item-options-delete-dialog.component.html'
})
export class LineItemOptionsDeleteDialogComponent {

    lineItemOptions: LineItemOptions;

    constructor(
        private lineItemOptionsService: LineItemOptionsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lineItemOptionsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'lineItemOptionsListModification',
                content: 'Deleted an lineItemOptions'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-line-item-options-delete-popup',
    template: ''
})
export class LineItemOptionsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lineItemOptionsPopupService: LineItemOptionsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.lineItemOptionsPopupService
                .open(LineItemOptionsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
