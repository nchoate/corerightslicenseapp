import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { LineItemOptionsComponent } from './line-item-options.component';
import { LineItemOptionsDetailComponent } from './line-item-options-detail.component';
import { LineItemOptionsPopupComponent } from './line-item-options-dialog.component';
import { LineItemOptionsDeletePopupComponent } from './line-item-options-delete-dialog.component';

export const lineItemOptionsRoute: Routes = [
    {
        path: 'line-item-options',
        component: LineItemOptionsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItemOptions'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'line-item-options/:id',
        component: LineItemOptionsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItemOptions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lineItemOptionsPopupRoute: Routes = [
    {
        path: 'line-item-options-new',
        component: LineItemOptionsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItemOptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'line-item-options/:id/edit',
        component: LineItemOptionsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItemOptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'line-item-options/:id/delete',
        component: LineItemOptionsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LineItemOptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
