import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LineItemOptions } from './line-item-options.model';
import { LineItemOptionsService } from './line-item-options.service';

@Component({
    selector: 'jhi-line-item-options-detail',
    templateUrl: './line-item-options-detail.component.html'
})
export class LineItemOptionsDetailComponent implements OnInit, OnDestroy {

    lineItemOptions: LineItemOptions;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private lineItemOptionsService: LineItemOptionsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLineItemOptions();
    }

    load(id) {
        this.lineItemOptionsService.find(id).subscribe((lineItemOptions) => {
            this.lineItemOptions = lineItemOptions;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLineItemOptions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'lineItemOptionsListModification',
            (response) => this.load(this.lineItemOptions.id)
        );
    }
}
