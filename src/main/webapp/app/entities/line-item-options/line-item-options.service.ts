import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { LineItemOptions } from './line-item-options.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class LineItemOptionsService {

    private resourceUrl = SERVER_API_URL + 'api/line-item-options';

    constructor(private http: Http) { }

    create(lineItemOptions: LineItemOptions): Observable<LineItemOptions> {
        const copy = this.convert(lineItemOptions);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(lineItemOptions: LineItemOptions): Observable<LineItemOptions> {
        const copy = this.convert(lineItemOptions);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<LineItemOptions> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LineItemOptions.
     */
    private convertItemFromServer(json: any): LineItemOptions {
        const entity: LineItemOptions = Object.assign(new LineItemOptions(), json);
        return entity;
    }

    /**
     * Convert a LineItemOptions to a JSON which can be sent to the server.
     */
    private convert(lineItemOptions: LineItemOptions): LineItemOptions {
        const copy: LineItemOptions = Object.assign({}, lineItemOptions);
        return copy;
    }
}
