import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreRightsLicenseAppSharedModule } from '../../shared';
import {
    LineItemOptionsService,
    LineItemOptionsPopupService,
    LineItemOptionsComponent,
    LineItemOptionsDetailComponent,
    LineItemOptionsDialogComponent,
    LineItemOptionsPopupComponent,
    LineItemOptionsDeletePopupComponent,
    LineItemOptionsDeleteDialogComponent,
    lineItemOptionsRoute,
    lineItemOptionsPopupRoute,
} from './';

const ENTITY_STATES = [
    ...lineItemOptionsRoute,
    ...lineItemOptionsPopupRoute,
];

@NgModule({
    imports: [
        CoreRightsLicenseAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LineItemOptionsComponent,
        LineItemOptionsDetailComponent,
        LineItemOptionsDialogComponent,
        LineItemOptionsDeleteDialogComponent,
        LineItemOptionsPopupComponent,
        LineItemOptionsDeletePopupComponent,
    ],
    entryComponents: [
        LineItemOptionsComponent,
        LineItemOptionsDialogComponent,
        LineItemOptionsPopupComponent,
        LineItemOptionsDeleteDialogComponent,
        LineItemOptionsDeletePopupComponent,
    ],
    providers: [
        LineItemOptionsService,
        LineItemOptionsPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreRightsLicenseAppLineItemOptionsModule {}
