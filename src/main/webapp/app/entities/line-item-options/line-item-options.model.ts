import { BaseEntity } from './../../shared';

export class LineItemOptions implements BaseEntity {
    constructor(
        public id?: number,
        public lineItemOptionId?: string,
        public name?: string,
        public helpDesc?: string,
        public ord?: number,
        public selected?: boolean,
        public lineItemId?: number,
    ) {
        this.selected = false;
    }
}
