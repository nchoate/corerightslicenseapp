import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LineItemOptions } from './line-item-options.model';
import { LineItemOptionsPopupService } from './line-item-options-popup.service';
import { LineItemOptionsService } from './line-item-options.service';
import { LineItem, LineItemService } from '../line-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-line-item-options-dialog',
    templateUrl: './line-item-options-dialog.component.html'
})
export class LineItemOptionsDialogComponent implements OnInit {

    lineItemOptions: LineItemOptions;
    isSaving: boolean;

    lineitems: LineItem[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private lineItemOptionsService: LineItemOptionsService,
        private lineItemService: LineItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.lineItemService.query()
            .subscribe((res: ResponseWrapper) => { this.lineitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lineItemOptions.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lineItemOptionsService.update(this.lineItemOptions));
        } else {
            this.subscribeToSaveResponse(
                this.lineItemOptionsService.create(this.lineItemOptions));
        }
    }

    private subscribeToSaveResponse(result: Observable<LineItemOptions>) {
        result.subscribe((res: LineItemOptions) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LineItemOptions) {
        this.eventManager.broadcast({ name: 'lineItemOptionsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackLineItemById(index: number, item: LineItem) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-line-item-options-popup',
    template: ''
})
export class LineItemOptionsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lineItemOptionsPopupService: LineItemOptionsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lineItemOptionsPopupService
                    .open(LineItemOptionsDialogComponent as Component, params['id']);
            } else {
                this.lineItemOptionsPopupService
                    .open(LineItemOptionsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
