import { BaseEntity } from './../../shared';

export class License implements BaseEntity {
    constructor(
        public id?: number,
        public licenseId?: string,
        public name?: string,
    ) {
    }
}
