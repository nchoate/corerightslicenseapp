import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreRightsLicenseAppSharedModule } from '../../shared';
import {
    LicenseService,
    LicensePopupService,
    LicenseComponent,
    LicenseDetailComponent,
    LicenseDialogComponent,
    LicensePopupComponent,
    LicenseDeletePopupComponent,
    LicenseDeleteDialogComponent,
    licenseRoute,
    licensePopupRoute,
} from './';

const ENTITY_STATES = [
    ...licenseRoute,
    ...licensePopupRoute,
];

@NgModule({
    imports: [
        CoreRightsLicenseAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LicenseComponent,
        LicenseDetailComponent,
        LicenseDialogComponent,
        LicenseDeleteDialogComponent,
        LicensePopupComponent,
        LicenseDeletePopupComponent,
    ],
    entryComponents: [
        LicenseComponent,
        LicenseDialogComponent,
        LicensePopupComponent,
        LicenseDeleteDialogComponent,
        LicenseDeletePopupComponent,
    ],
    providers: [
        LicenseService,
        LicensePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreRightsLicenseAppLicenseModule {}
