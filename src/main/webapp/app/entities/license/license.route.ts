import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { LicenseComponent } from './license.component';
import { LicenseDetailComponent } from './license-detail.component';
import { LicensePopupComponent } from './license-dialog.component';
import { LicenseDeletePopupComponent } from './license-delete-dialog.component';

export const licenseRoute: Routes = [
    {
        path: 'license',
        component: LicenseComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Licenses'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'license/:id',
        component: LicenseDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Licenses'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const licensePopupRoute: Routes = [
    {
        path: 'license-new',
        component: LicensePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Licenses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'license/:id/edit',
        component: LicensePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Licenses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'license/:id/delete',
        component: LicenseDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Licenses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
