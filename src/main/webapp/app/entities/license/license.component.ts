import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { License } from './license.model';
import { LicenseService } from './license.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-license',
    templateUrl: './license.component.html'
})
export class LicenseComponent implements OnInit, OnDestroy {
licenses: License[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private licenseService: LicenseService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.licenseService.query().subscribe(
            (res: ResponseWrapper) => {
                this.licenses = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLicenses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: License) {
        return item.id;
    }
    registerChangeInLicenses() {
        this.eventSubscriber = this.eventManager.subscribe('licenseListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
