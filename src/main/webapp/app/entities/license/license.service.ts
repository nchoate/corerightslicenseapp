import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { License } from './license.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class LicenseService {

    private resourceUrl = SERVER_API_URL + 'api/licenses';

    constructor(private http: Http) { }

    create(license: License): Observable<License> {
        const copy = this.convert(license);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(license: License): Observable<License> {
        const copy = this.convert(license);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<License> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to License.
     */
    private convertItemFromServer(json: any): License {
        const entity: License = Object.assign(new License(), json);
        return entity;
    }

    /**
     * Convert a License to a JSON which can be sent to the server.
     */
    private convert(license: License): License {
        const copy: License = Object.assign({}, license);
        return copy;
    }
}
