import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CoreRightsLicenseAppLicenseModule } from './license/license.module';
import { CoreRightsLicenseAppSectionModule } from './section/section.module';
import { CoreRightsLicenseAppLineItemModule } from './line-item/line-item.module';
import { CoreRightsLicenseAppLineItemOptionsModule } from './line-item-options/line-item-options.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CoreRightsLicenseAppLicenseModule,
        CoreRightsLicenseAppSectionModule,
        CoreRightsLicenseAppLineItemModule,
        CoreRightsLicenseAppLineItemOptionsModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreRightsLicenseAppEntityModule {}
