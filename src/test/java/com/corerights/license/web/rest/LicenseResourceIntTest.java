package com.corerights.license.web.rest;

import static com.corerights.license.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.corerights.license.CoreRightsLicenseApp;
import com.corerights.license.domain.License;
import com.corerights.license.repository.LicenseRepository;
import com.corerights.license.service.LicenseService;
import com.corerights.license.service.dto.LicenseDTO;
import com.corerights.license.service.mapper.LicenseMappr;
import com.corerights.license.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the LicenseResource REST controller.
 *
 * @see LicenseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreRightsLicenseApp.class)
public class LicenseResourceIntTest {

    private static final String DEFAULT_LICENSE_ID = "AAAAAAAAAA";
    private static final String UPDATED_LICENSE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private LicenseMappr licenseMapper;

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLicenseMockMvc;

    private License license;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LicenseResource licenseResource = new LicenseResource(licenseService);
        this.restLicenseMockMvc = MockMvcBuilders.standaloneSetup(licenseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static License createEntity(EntityManager em) {
        License license = new License()
            .licenseId(DEFAULT_LICENSE_ID)
            .name(DEFAULT_NAME);
        return license;
    }

    @Before
    public void initTest() {
        license = createEntity(em);
    }

    @Test
    @Transactional
    public void createLicense() throws Exception {
        int databaseSizeBeforeCreate = licenseRepository.findAll().size();

        // Create the License
        LicenseDTO licenseDTO = licenseMapper.toDto(license);
        restLicenseMockMvc.perform(post("/api/licenses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(licenseDTO)))
            .andExpect(status().isCreated());

        // Validate the License in the database
        List<License> licenseList = licenseRepository.findAll();
        assertThat(licenseList).hasSize(databaseSizeBeforeCreate + 1);
        License testLicense = licenseList.get(licenseList.size() - 1);
        assertThat(testLicense.getLicenseId()).isEqualTo(DEFAULT_LICENSE_ID);
        assertThat(testLicense.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createLicenseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = licenseRepository.findAll().size();

        // Create the License with an existing ID
        license.setId(1L);
        LicenseDTO licenseDTO = licenseMapper.toDto(license);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLicenseMockMvc.perform(post("/api/licenses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(licenseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the License in the database
        List<License> licenseList = licenseRepository.findAll();
        assertThat(licenseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLicenses() throws Exception {
        // Initialize the database
        licenseRepository.saveAndFlush(license);

        // Get all the licenseList
        restLicenseMockMvc.perform(get("/api/licenses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(license.getId().intValue())))
            .andExpect(jsonPath("$.[*].licenseId").value(hasItem(DEFAULT_LICENSE_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getLicense() throws Exception {
        // Initialize the database
        licenseRepository.saveAndFlush(license);

        // Get the license
        restLicenseMockMvc.perform(get("/api/licenses/{id}", license.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(license.getId().intValue()))
            .andExpect(jsonPath("$.licenseId").value(DEFAULT_LICENSE_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLicense() throws Exception {
        // Get the license
        restLicenseMockMvc.perform(get("/api/licenses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLicense() throws Exception {
        // Initialize the database
        licenseRepository.saveAndFlush(license);
        int databaseSizeBeforeUpdate = licenseRepository.findAll().size();

        // Update the license
        License updatedLicense = licenseRepository.findOne(license.getId());
        // Disconnect from session so that the updates on updatedLicense are not directly saved in db
        em.detach(updatedLicense);
        updatedLicense
            .licenseId(UPDATED_LICENSE_ID)
            .name(UPDATED_NAME);
        LicenseDTO licenseDTO = licenseMapper.toDto(updatedLicense);

        restLicenseMockMvc.perform(put("/api/licenses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(licenseDTO)))
            .andExpect(status().isOk());

        // Validate the License in the database
        List<License> licenseList = licenseRepository.findAll();
        assertThat(licenseList).hasSize(databaseSizeBeforeUpdate);
        License testLicense = licenseList.get(licenseList.size() - 1);
        assertThat(testLicense.getLicenseId()).isEqualTo(UPDATED_LICENSE_ID);
        assertThat(testLicense.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingLicense() throws Exception {
        int databaseSizeBeforeUpdate = licenseRepository.findAll().size();

        // Create the License
        LicenseDTO licenseDTO = licenseMapper.toDto(license);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLicenseMockMvc.perform(put("/api/licenses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(licenseDTO)))
            .andExpect(status().isCreated());

        // Validate the License in the database
        List<License> licenseList = licenseRepository.findAll();
        assertThat(licenseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLicense() throws Exception {
        // Initialize the database
        licenseRepository.saveAndFlush(license);
        int databaseSizeBeforeDelete = licenseRepository.findAll().size();

        // Get the license
        restLicenseMockMvc.perform(delete("/api/licenses/{id}", license.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<License> licenseList = licenseRepository.findAll();
        assertThat(licenseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(License.class);
        License license1 = new License();
        license1.setId(1L);
        License license2 = new License();
        license2.setId(license1.getId());
        assertThat(license1).isEqualTo(license2);
        license2.setId(2L);
        assertThat(license1).isNotEqualTo(license2);
        license1.setId(null);
        assertThat(license1).isNotEqualTo(license2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LicenseDTO.class);
        LicenseDTO licenseDTO1 = new LicenseDTO();
        licenseDTO1.setId(1L);
        LicenseDTO licenseDTO2 = new LicenseDTO();
        assertThat(licenseDTO1).isNotEqualTo(licenseDTO2);
        licenseDTO2.setId(licenseDTO1.getId());
        assertThat(licenseDTO1).isEqualTo(licenseDTO2);
        licenseDTO2.setId(2L);
        assertThat(licenseDTO1).isNotEqualTo(licenseDTO2);
        licenseDTO1.setId(null);
        assertThat(licenseDTO1).isNotEqualTo(licenseDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(licenseMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(licenseMapper.fromId(null)).isNull();
    }
}
