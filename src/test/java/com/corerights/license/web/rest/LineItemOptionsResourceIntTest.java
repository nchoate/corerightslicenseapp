package com.corerights.license.web.rest;

import com.corerights.license.CoreRightsLicenseApp;

import com.corerights.license.domain.LineItemOptions;
import com.corerights.license.repository.LineItemOptionsRepository;
import com.corerights.license.service.LineItemOptionsService;
import com.corerights.license.service.dto.LineItemOptionsDTO;
import com.corerights.license.service.mapper.LineItemOptionsMapper;
import com.corerights.license.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.corerights.license.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LineItemOptionsResource REST controller.
 *
 * @see LineItemOptionsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreRightsLicenseApp.class)
public class LineItemOptionsResourceIntTest {

    private static final String DEFAULT_LINE_ITEM_OPTION_ID = "AAAAAAAAAA";
    private static final String UPDATED_LINE_ITEM_OPTION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_HELP_DESC = "AAAAAAAAAA";
    private static final String UPDATED_HELP_DESC = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORD = 1;
    private static final Integer UPDATED_ORD = 2;

    private static final Boolean DEFAULT_SELECTED = false;
    private static final Boolean UPDATED_SELECTED = true;

    @Autowired
    private LineItemOptionsRepository lineItemOptionsRepository;

    @Autowired
    private LineItemOptionsMapper lineItemOptionsMapper;

    @Autowired
    private LineItemOptionsService lineItemOptionsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLineItemOptionsMockMvc;

    private LineItemOptions lineItemOptions;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LineItemOptionsResource lineItemOptionsResource = new LineItemOptionsResource(lineItemOptionsService);
        this.restLineItemOptionsMockMvc = MockMvcBuilders.standaloneSetup(lineItemOptionsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LineItemOptions createEntity(EntityManager em) {
        LineItemOptions lineItemOptions = new LineItemOptions()
            .lineItemOptionId(DEFAULT_LINE_ITEM_OPTION_ID)
            .name(DEFAULT_NAME)
            .helpDesc(DEFAULT_HELP_DESC)
            .ord(DEFAULT_ORD)
            .selected(DEFAULT_SELECTED);
        return lineItemOptions;
    }

    @Before
    public void initTest() {
        lineItemOptions = createEntity(em);
    }

    @Test
    @Transactional
    public void createLineItemOptions() throws Exception {
        int databaseSizeBeforeCreate = lineItemOptionsRepository.findAll().size();

        // Create the LineItemOptions
        LineItemOptionsDTO lineItemOptionsDTO = lineItemOptionsMapper.toDto(lineItemOptions);
        restLineItemOptionsMockMvc.perform(post("/api/line-item-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemOptionsDTO)))
            .andExpect(status().isCreated());

        // Validate the LineItemOptions in the database
        List<LineItemOptions> lineItemOptionsList = lineItemOptionsRepository.findAll();
        assertThat(lineItemOptionsList).hasSize(databaseSizeBeforeCreate + 1);
        LineItemOptions testLineItemOptions = lineItemOptionsList.get(lineItemOptionsList.size() - 1);
        assertThat(testLineItemOptions.getLineItemOptionId()).isEqualTo(DEFAULT_LINE_ITEM_OPTION_ID);
        assertThat(testLineItemOptions.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testLineItemOptions.getHelpDesc()).isEqualTo(DEFAULT_HELP_DESC);
        assertThat(testLineItemOptions.getOrd()).isEqualTo(DEFAULT_ORD);
        assertThat(testLineItemOptions.isSelected()).isEqualTo(DEFAULT_SELECTED);
    }

    @Test
    @Transactional
    public void createLineItemOptionsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lineItemOptionsRepository.findAll().size();

        // Create the LineItemOptions with an existing ID
        lineItemOptions.setId(1L);
        LineItemOptionsDTO lineItemOptionsDTO = lineItemOptionsMapper.toDto(lineItemOptions);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLineItemOptionsMockMvc.perform(post("/api/line-item-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemOptionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LineItemOptions in the database
        List<LineItemOptions> lineItemOptionsList = lineItemOptionsRepository.findAll();
        assertThat(lineItemOptionsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLineItemOptions() throws Exception {
        // Initialize the database
        lineItemOptionsRepository.saveAndFlush(lineItemOptions);

        // Get all the lineItemOptionsList
        restLineItemOptionsMockMvc.perform(get("/api/line-item-options?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lineItemOptions.getId().intValue())))
            .andExpect(jsonPath("$.[*].lineItemOptionId").value(hasItem(DEFAULT_LINE_ITEM_OPTION_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].helpDesc").value(hasItem(DEFAULT_HELP_DESC.toString())))
            .andExpect(jsonPath("$.[*].ord").value(hasItem(DEFAULT_ORD)))
            .andExpect(jsonPath("$.[*].selected").value(hasItem(DEFAULT_SELECTED.booleanValue())));
    }

    @Test
    @Transactional
    public void getLineItemOptions() throws Exception {
        // Initialize the database
        lineItemOptionsRepository.saveAndFlush(lineItemOptions);

        // Get the lineItemOptions
        restLineItemOptionsMockMvc.perform(get("/api/line-item-options/{id}", lineItemOptions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lineItemOptions.getId().intValue()))
            .andExpect(jsonPath("$.lineItemOptionId").value(DEFAULT_LINE_ITEM_OPTION_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.helpDesc").value(DEFAULT_HELP_DESC.toString()))
            .andExpect(jsonPath("$.ord").value(DEFAULT_ORD))
            .andExpect(jsonPath("$.selected").value(DEFAULT_SELECTED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLineItemOptions() throws Exception {
        // Get the lineItemOptions
        restLineItemOptionsMockMvc.perform(get("/api/line-item-options/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLineItemOptions() throws Exception {
        // Initialize the database
        lineItemOptionsRepository.saveAndFlush(lineItemOptions);
        int databaseSizeBeforeUpdate = lineItemOptionsRepository.findAll().size();

        // Update the lineItemOptions
        LineItemOptions updatedLineItemOptions = lineItemOptionsRepository.findOne(lineItemOptions.getId());
        // Disconnect from session so that the updates on updatedLineItemOptions are not directly saved in db
        em.detach(updatedLineItemOptions);
        updatedLineItemOptions
            .lineItemOptionId(UPDATED_LINE_ITEM_OPTION_ID)
            .name(UPDATED_NAME)
            .helpDesc(UPDATED_HELP_DESC)
            .ord(UPDATED_ORD)
            .selected(UPDATED_SELECTED);
        LineItemOptionsDTO lineItemOptionsDTO = lineItemOptionsMapper.toDto(updatedLineItemOptions);

        restLineItemOptionsMockMvc.perform(put("/api/line-item-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemOptionsDTO)))
            .andExpect(status().isOk());

        // Validate the LineItemOptions in the database
        List<LineItemOptions> lineItemOptionsList = lineItemOptionsRepository.findAll();
        assertThat(lineItemOptionsList).hasSize(databaseSizeBeforeUpdate);
        LineItemOptions testLineItemOptions = lineItemOptionsList.get(lineItemOptionsList.size() - 1);
        assertThat(testLineItemOptions.getLineItemOptionId()).isEqualTo(UPDATED_LINE_ITEM_OPTION_ID);
        assertThat(testLineItemOptions.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLineItemOptions.getHelpDesc()).isEqualTo(UPDATED_HELP_DESC);
        assertThat(testLineItemOptions.getOrd()).isEqualTo(UPDATED_ORD);
        assertThat(testLineItemOptions.isSelected()).isEqualTo(UPDATED_SELECTED);
    }

    @Test
    @Transactional
    public void updateNonExistingLineItemOptions() throws Exception {
        int databaseSizeBeforeUpdate = lineItemOptionsRepository.findAll().size();

        // Create the LineItemOptions
        LineItemOptionsDTO lineItemOptionsDTO = lineItemOptionsMapper.toDto(lineItemOptions);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLineItemOptionsMockMvc.perform(put("/api/line-item-options")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemOptionsDTO)))
            .andExpect(status().isCreated());

        // Validate the LineItemOptions in the database
        List<LineItemOptions> lineItemOptionsList = lineItemOptionsRepository.findAll();
        assertThat(lineItemOptionsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLineItemOptions() throws Exception {
        // Initialize the database
        lineItemOptionsRepository.saveAndFlush(lineItemOptions);
        int databaseSizeBeforeDelete = lineItemOptionsRepository.findAll().size();

        // Get the lineItemOptions
        restLineItemOptionsMockMvc.perform(delete("/api/line-item-options/{id}", lineItemOptions.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LineItemOptions> lineItemOptionsList = lineItemOptionsRepository.findAll();
        assertThat(lineItemOptionsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LineItemOptions.class);
        LineItemOptions lineItemOptions1 = new LineItemOptions();
        lineItemOptions1.setId(1L);
        LineItemOptions lineItemOptions2 = new LineItemOptions();
        lineItemOptions2.setId(lineItemOptions1.getId());
        assertThat(lineItemOptions1).isEqualTo(lineItemOptions2);
        lineItemOptions2.setId(2L);
        assertThat(lineItemOptions1).isNotEqualTo(lineItemOptions2);
        lineItemOptions1.setId(null);
        assertThat(lineItemOptions1).isNotEqualTo(lineItemOptions2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LineItemOptionsDTO.class);
        LineItemOptionsDTO lineItemOptionsDTO1 = new LineItemOptionsDTO();
        lineItemOptionsDTO1.setId(1L);
        LineItemOptionsDTO lineItemOptionsDTO2 = new LineItemOptionsDTO();
        assertThat(lineItemOptionsDTO1).isNotEqualTo(lineItemOptionsDTO2);
        lineItemOptionsDTO2.setId(lineItemOptionsDTO1.getId());
        assertThat(lineItemOptionsDTO1).isEqualTo(lineItemOptionsDTO2);
        lineItemOptionsDTO2.setId(2L);
        assertThat(lineItemOptionsDTO1).isNotEqualTo(lineItemOptionsDTO2);
        lineItemOptionsDTO1.setId(null);
        assertThat(lineItemOptionsDTO1).isNotEqualTo(lineItemOptionsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(lineItemOptionsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(lineItemOptionsMapper.fromId(null)).isNull();
    }
}
