package com.corerights.license.web.rest;

import com.corerights.license.CoreRightsLicenseApp;

import com.corerights.license.domain.LineItem;
import com.corerights.license.repository.LineItemRepository;
import com.corerights.license.service.LineItemService;
import com.corerights.license.service.dto.LineItemDTO;
import com.corerights.license.service.mapper.LineItemMapper;
import com.corerights.license.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.corerights.license.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.corerights.license.domain.enumeration.InputType;
/**
 * Test class for the LineItemResource REST controller.
 *
 * @see LineItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreRightsLicenseApp.class)
public class LineItemResourceIntTest {

    private static final String DEFAULT_LINE_ITEM_ID = "AAAAAAAAAA";
    private static final String UPDATED_LINE_ITEM_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_HELP_DESC = "AAAAAAAAAA";
    private static final String UPDATED_HELP_DESC = "BBBBBBBBBB";

    private static final InputType DEFAULT_INPUT_TYPE = InputType.TEXT;
    private static final InputType UPDATED_INPUT_TYPE = InputType.SINGLE_SELECT;

    private static final String DEFAULT_USER_RESPONSE = "AAAAAAAAAA";
    private static final String UPDATED_USER_RESPONSE = "BBBBBBBBBB";

    @Autowired
    private LineItemRepository lineItemRepository;

    @Autowired
    private LineItemMapper lineItemMapper;

    @Autowired
    private LineItemService lineItemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLineItemMockMvc;

    private LineItem lineItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LineItemResource lineItemResource = new LineItemResource(lineItemService);
        this.restLineItemMockMvc = MockMvcBuilders.standaloneSetup(lineItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LineItem createEntity(EntityManager em) {
        LineItem lineItem = new LineItem()
            .lineItemId(DEFAULT_LINE_ITEM_ID)
            .name(DEFAULT_NAME)
            .helpDesc(DEFAULT_HELP_DESC)
            .inputType(DEFAULT_INPUT_TYPE)
            .userResponse(DEFAULT_USER_RESPONSE);
        return lineItem;
    }

    @Before
    public void initTest() {
        lineItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createLineItem() throws Exception {
        int databaseSizeBeforeCreate = lineItemRepository.findAll().size();

        // Create the LineItem
        LineItemDTO lineItemDTO = lineItemMapper.toDto(lineItem);
        restLineItemMockMvc.perform(post("/api/line-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemDTO)))
            .andExpect(status().isCreated());

        // Validate the LineItem in the database
        List<LineItem> lineItemList = lineItemRepository.findAll();
        assertThat(lineItemList).hasSize(databaseSizeBeforeCreate + 1);
        LineItem testLineItem = lineItemList.get(lineItemList.size() - 1);
        assertThat(testLineItem.getLineItemId()).isEqualTo(DEFAULT_LINE_ITEM_ID);
        assertThat(testLineItem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testLineItem.getHelpDesc()).isEqualTo(DEFAULT_HELP_DESC);
        assertThat(testLineItem.getInputType()).isEqualTo(DEFAULT_INPUT_TYPE);
        assertThat(testLineItem.getUserResponse()).isEqualTo(DEFAULT_USER_RESPONSE);
    }

    @Test
    @Transactional
    public void createLineItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lineItemRepository.findAll().size();

        // Create the LineItem with an existing ID
        lineItem.setId(1L);
        LineItemDTO lineItemDTO = lineItemMapper.toDto(lineItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLineItemMockMvc.perform(post("/api/line-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LineItem in the database
        List<LineItem> lineItemList = lineItemRepository.findAll();
        assertThat(lineItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLineItems() throws Exception {
        // Initialize the database
        lineItemRepository.saveAndFlush(lineItem);

        // Get all the lineItemList
        restLineItemMockMvc.perform(get("/api/line-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lineItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].lineItemId").value(hasItem(DEFAULT_LINE_ITEM_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].helpDesc").value(hasItem(DEFAULT_HELP_DESC.toString())))
            .andExpect(jsonPath("$.[*].inputType").value(hasItem(DEFAULT_INPUT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].userResponse").value(hasItem(DEFAULT_USER_RESPONSE.toString())));
    }

    @Test
    @Transactional
    public void getLineItem() throws Exception {
        // Initialize the database
        lineItemRepository.saveAndFlush(lineItem);

        // Get the lineItem
        restLineItemMockMvc.perform(get("/api/line-items/{id}", lineItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lineItem.getId().intValue()))
            .andExpect(jsonPath("$.lineItemId").value(DEFAULT_LINE_ITEM_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.helpDesc").value(DEFAULT_HELP_DESC.toString()))
            .andExpect(jsonPath("$.inputType").value(DEFAULT_INPUT_TYPE.toString()))
            .andExpect(jsonPath("$.userResponse").value(DEFAULT_USER_RESPONSE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLineItem() throws Exception {
        // Get the lineItem
        restLineItemMockMvc.perform(get("/api/line-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLineItem() throws Exception {
        // Initialize the database
        lineItemRepository.saveAndFlush(lineItem);
        int databaseSizeBeforeUpdate = lineItemRepository.findAll().size();

        // Update the lineItem
        LineItem updatedLineItem = lineItemRepository.findOne(lineItem.getId());
        // Disconnect from session so that the updates on updatedLineItem are not directly saved in db
        em.detach(updatedLineItem);
        updatedLineItem
            .lineItemId(UPDATED_LINE_ITEM_ID)
            .name(UPDATED_NAME)
            .helpDesc(UPDATED_HELP_DESC)
            .inputType(UPDATED_INPUT_TYPE)
            .userResponse(UPDATED_USER_RESPONSE);
        LineItemDTO lineItemDTO = lineItemMapper.toDto(updatedLineItem);

        restLineItemMockMvc.perform(put("/api/line-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemDTO)))
            .andExpect(status().isOk());

        // Validate the LineItem in the database
        List<LineItem> lineItemList = lineItemRepository.findAll();
        assertThat(lineItemList).hasSize(databaseSizeBeforeUpdate);
        LineItem testLineItem = lineItemList.get(lineItemList.size() - 1);
        assertThat(testLineItem.getLineItemId()).isEqualTo(UPDATED_LINE_ITEM_ID);
        assertThat(testLineItem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLineItem.getHelpDesc()).isEqualTo(UPDATED_HELP_DESC);
        assertThat(testLineItem.getInputType()).isEqualTo(UPDATED_INPUT_TYPE);
        assertThat(testLineItem.getUserResponse()).isEqualTo(UPDATED_USER_RESPONSE);
    }

    @Test
    @Transactional
    public void updateNonExistingLineItem() throws Exception {
        int databaseSizeBeforeUpdate = lineItemRepository.findAll().size();

        // Create the LineItem
        LineItemDTO lineItemDTO = lineItemMapper.toDto(lineItem);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLineItemMockMvc.perform(put("/api/line-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lineItemDTO)))
            .andExpect(status().isCreated());

        // Validate the LineItem in the database
        List<LineItem> lineItemList = lineItemRepository.findAll();
        assertThat(lineItemList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLineItem() throws Exception {
        // Initialize the database
        lineItemRepository.saveAndFlush(lineItem);
        int databaseSizeBeforeDelete = lineItemRepository.findAll().size();

        // Get the lineItem
        restLineItemMockMvc.perform(delete("/api/line-items/{id}", lineItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LineItem> lineItemList = lineItemRepository.findAll();
        assertThat(lineItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LineItem.class);
        LineItem lineItem1 = new LineItem();
        lineItem1.setId(1L);
        LineItem lineItem2 = new LineItem();
        lineItem2.setId(lineItem1.getId());
        assertThat(lineItem1).isEqualTo(lineItem2);
        lineItem2.setId(2L);
        assertThat(lineItem1).isNotEqualTo(lineItem2);
        lineItem1.setId(null);
        assertThat(lineItem1).isNotEqualTo(lineItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LineItemDTO.class);
        LineItemDTO lineItemDTO1 = new LineItemDTO();
        lineItemDTO1.setId(1L);
        LineItemDTO lineItemDTO2 = new LineItemDTO();
        assertThat(lineItemDTO1).isNotEqualTo(lineItemDTO2);
        lineItemDTO2.setId(lineItemDTO1.getId());
        assertThat(lineItemDTO1).isEqualTo(lineItemDTO2);
        lineItemDTO2.setId(2L);
        assertThat(lineItemDTO1).isNotEqualTo(lineItemDTO2);
        lineItemDTO1.setId(null);
        assertThat(lineItemDTO1).isNotEqualTo(lineItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(lineItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(lineItemMapper.fromId(null)).isNull();
    }
}
