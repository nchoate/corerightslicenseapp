/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { CoreRightsLicenseAppTestModule } from '../../../test.module';
import { LineItemOptionsComponent } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.component';
import { LineItemOptionsService } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.service';
import { LineItemOptions } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.model';

describe('Component Tests', () => {

    describe('LineItemOptions Management Component', () => {
        let comp: LineItemOptionsComponent;
        let fixture: ComponentFixture<LineItemOptionsComponent>;
        let service: LineItemOptionsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CoreRightsLicenseAppTestModule],
                declarations: [LineItemOptionsComponent],
                providers: [
                    LineItemOptionsService
                ]
            })
            .overrideTemplate(LineItemOptionsComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LineItemOptionsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineItemOptionsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new LineItemOptions(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.lineItemOptions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
