/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CoreRightsLicenseAppTestModule } from '../../../test.module';
import { LineItemOptionsDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options-delete-dialog.component';
import { LineItemOptionsService } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.service';

describe('Component Tests', () => {

    describe('LineItemOptions Management Delete Component', () => {
        let comp: LineItemOptionsDeleteDialogComponent;
        let fixture: ComponentFixture<LineItemOptionsDeleteDialogComponent>;
        let service: LineItemOptionsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CoreRightsLicenseAppTestModule],
                declarations: [LineItemOptionsDeleteDialogComponent],
                providers: [
                    LineItemOptionsService
                ]
            })
            .overrideTemplate(LineItemOptionsDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LineItemOptionsDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineItemOptionsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
