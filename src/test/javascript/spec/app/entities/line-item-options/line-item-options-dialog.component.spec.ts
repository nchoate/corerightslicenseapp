/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CoreRightsLicenseAppTestModule } from '../../../test.module';
import { LineItemOptionsDialogComponent } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options-dialog.component';
import { LineItemOptionsService } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.service';
import { LineItemOptions } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.model';
import { LineItemService } from '../../../../../../main/webapp/app/entities/line-item';

describe('Component Tests', () => {

    describe('LineItemOptions Management Dialog Component', () => {
        let comp: LineItemOptionsDialogComponent;
        let fixture: ComponentFixture<LineItemOptionsDialogComponent>;
        let service: LineItemOptionsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CoreRightsLicenseAppTestModule],
                declarations: [LineItemOptionsDialogComponent],
                providers: [
                    LineItemService,
                    LineItemOptionsService
                ]
            })
            .overrideTemplate(LineItemOptionsDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LineItemOptionsDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineItemOptionsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LineItemOptions(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.lineItemOptions = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'lineItemOptionsListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LineItemOptions();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.lineItemOptions = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'lineItemOptionsListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
