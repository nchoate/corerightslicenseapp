/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { CoreRightsLicenseAppTestModule } from '../../../test.module';
import { LineItemOptionsDetailComponent } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options-detail.component';
import { LineItemOptionsService } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.service';
import { LineItemOptions } from '../../../../../../main/webapp/app/entities/line-item-options/line-item-options.model';

describe('Component Tests', () => {

    describe('LineItemOptions Management Detail Component', () => {
        let comp: LineItemOptionsDetailComponent;
        let fixture: ComponentFixture<LineItemOptionsDetailComponent>;
        let service: LineItemOptionsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CoreRightsLicenseAppTestModule],
                declarations: [LineItemOptionsDetailComponent],
                providers: [
                    LineItemOptionsService
                ]
            })
            .overrideTemplate(LineItemOptionsDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LineItemOptionsDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineItemOptionsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new LineItemOptions(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.lineItemOptions).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
