/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { CoreRightsLicenseAppTestModule } from '../../../test.module';
import { LineItemComponent } from '../../../../../../main/webapp/app/entities/line-item/line-item.component';
import { LineItemService } from '../../../../../../main/webapp/app/entities/line-item/line-item.service';
import { LineItem } from '../../../../../../main/webapp/app/entities/line-item/line-item.model';

describe('Component Tests', () => {

    describe('LineItem Management Component', () => {
        let comp: LineItemComponent;
        let fixture: ComponentFixture<LineItemComponent>;
        let service: LineItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CoreRightsLicenseAppTestModule],
                declarations: [LineItemComponent],
                providers: [
                    LineItemService
                ]
            })
            .overrideTemplate(LineItemComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LineItemComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new LineItem(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.lineItems[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
