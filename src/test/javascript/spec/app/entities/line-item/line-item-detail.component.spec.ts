/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { CoreRightsLicenseAppTestModule } from '../../../test.module';
import { LineItemDetailComponent } from '../../../../../../main/webapp/app/entities/line-item/line-item-detail.component';
import { LineItemService } from '../../../../../../main/webapp/app/entities/line-item/line-item.service';
import { LineItem } from '../../../../../../main/webapp/app/entities/line-item/line-item.model';

describe('Component Tests', () => {

    describe('LineItem Management Detail Component', () => {
        let comp: LineItemDetailComponent;
        let fixture: ComponentFixture<LineItemDetailComponent>;
        let service: LineItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CoreRightsLicenseAppTestModule],
                declarations: [LineItemDetailComponent],
                providers: [
                    LineItemService
                ]
            })
            .overrideTemplate(LineItemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LineItemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LineItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new LineItem(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.lineItem).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
